package main

import (
	t11 "gitlab.com/pythias/t1"
	t21 "gitlab.com/pythias/t2"
)

func main() {
	t1 := t11.T1{}
	t2 := t21.T2{}

	t1.Hello()
	t2.Say()
}
