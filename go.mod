module gitlab.com/pythias/t3

go 1.17

require (
	gitlab.com/pythias/t1 v1.0.1 // indirect
	gitlab.com/pythias/t2 v1.0.0 // indirect
)
